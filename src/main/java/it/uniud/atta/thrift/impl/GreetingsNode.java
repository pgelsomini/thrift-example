package it.uniud.atta.thrift.impl;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TSocket;

import it.uniud.atta.thrift.generated.ManagementService;
import it.uniud.atta.thrift.generated.NodeManifest;
import it.uniud.atta.thrift.generated.User;

public class GreetingsNode implements NodeIface {

	private ThriftServer helloServer;
	private ManagementService.Client managementClient;
	private NodeManifest nodeManifest;
	private User user;
	private String name;
	private MessageDispatcher messenger;
	
	public GreetingsNode(NodeManifest nodeManifest, String name) {
		this.nodeManifest = nodeManifest;
		this.helloServer = new ThriftServer(GreetingsServiceHandler.class);
		this.name = name;
	}

	@Override
	public void turnOn() throws TException {
		
		helloServer.start();
		
		TSocket helloServerTransport = new TSocket("localhost", Constants.SERVER_PORT);
		TBinaryProtocol helloServerProtocol = new TBinaryProtocol(helloServerTransport);
		managementClient = new ManagementService.Client(helloServerProtocol);
		this.setUser(new User(name));
		this.setMessenger(new MessageDispatcher(managementClient));
		helloServerTransport.open();
		
		helloServer = new ThriftServer(GreetingsServiceHandler.class);
		PortNegotiator negotiator = new PortNegotiator(managementClient);
		negotiator.negotiate(nodeManifest, helloServer);
	}

	@Override
	public void turnOff() {
		helloServer.stop();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public MessageDispatcher getMessenger() {
		return messenger;
	}

	public void setMessenger(MessageDispatcher messenger) {
		this.messenger = messenger;
	}
}