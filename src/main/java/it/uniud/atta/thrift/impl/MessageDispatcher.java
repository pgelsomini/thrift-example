package it.uniud.atta.thrift.impl;

import java.util.List;
import java.util.Random;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

import it.uniud.atta.thrift.generated.GreetingsService;
import it.uniud.atta.thrift.generated.ManagementService;
import it.uniud.atta.thrift.generated.Message;
import it.uniud.atta.thrift.generated.ServiceProvider;
import it.uniud.atta.thrift.generated.User;

public class MessageDispatcher {
	
	private static Random random = new Random(10);
	private ManagementService.Client managementClient;
	private GreetingsService.Client helloClient;
	
	public MessageDispatcher(ManagementService.Client managementClient) {
		this.managementClient = managementClient;
	}
	
	public Message callOtherNode(User user) throws TException {
		
		ServiceProvider provider = null;
		List<ServiceProvider> providers = managementClient.getProvidersForService(user.name);
		
		if (providers.isEmpty())
			throw new NoProviderAvailableException();
		else {
			provider = providers.get(random.nextInt(providers.size()));
			
			TTransport helloTransport = new TSocket(provider.getHostName(), provider.getPort());
			TProtocol helloProtocol = new TBinaryProtocol(helloTransport);
			helloClient = new GreetingsService.Client(helloProtocol);
			helloTransport.open();
			
			return helloClient.getHello(user);
		}
	}
	
	
	
}

