package it.uniud.atta.thrift.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Random;

import org.apache.thrift.TException;

import it.uniud.atta.thrift.generated.ManagementService;
import it.uniud.atta.thrift.generated.NodeManifest;
import it.uniud.atta.thrift.generated.ServiceProvider;

public class ManagementServiceHandler implements ManagementService.Iface {

    private final Map<NodeManifest, Integer> nodePorts;
    private final Random rnd;
    
    public ManagementServiceHandler() 
    {
        nodePorts = new HashMap<NodeManifest, Integer>();
        rnd = new Random();
    }

    public int registerNode(NodeManifest hostAndService, int proposedPort) throws TException {
		
    	int chosenPort = proposedPort;
    	
    	if (isPortAvailable(proposedPort)) {
    		nodePorts.put(hostAndService, proposedPort);
    	} else {
    		chosenPort = getAvailablePort();
    	}
    		
		return chosenPort;
    }
	
	public List<ServiceProvider> getProvidersForService(String serviceName) throws TException {
		List<ServiceProvider> result = new ArrayList<>();
		for(Map.Entry<NodeManifest, Integer> pair : nodePorts.entrySet()) {	
			List<String> services = pair.getKey().serviceList;
			if (services.contains(serviceName)) {
				result.add(new ServiceProvider(pair.getKey().hostName, pair.getValue()));
				}
			}
		return result;
	}
		
    public List<NodeManifest> getAllNodes() throws TException {	
      	return new ArrayList<NodeManifest>(nodePorts.keySet());
	}

	
	public void unRegister(NodeManifest hostAndService) throws TException {
		nodePorts.remove(hostAndService);
		return;
	}
	
	private boolean isPortAvailable(int port) {
		if (port < 1025)
			return false;
		for (Integer usedPort : nodePorts.values()) 
			if (port == usedPort)
				return false;
		return true;
	}
	
	private int getAvailablePort() {
		int result;
		do {
			result = 1025 + rnd.nextInt(65535-1025);
		} while (!isPortAvailable(result));
		return result;
	}
}