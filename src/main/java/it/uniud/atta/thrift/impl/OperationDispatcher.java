package it.uniud.atta.thrift.impl;

import it.uniud.atta.thrift.generated.ArithmeticService;
import it.uniud.atta.thrift.generated.ManagementService;
import it.uniud.atta.thrift.generated.ServiceProvider;

import java.util.List;
import java.util.Random;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

	public class OperationDispatcher {
		
		private static Random random = new Random(10);
		
		public OperationDispatcher(ManagementService.Client managementClient) {
			this.managementClient = managementClient;
		}
		
		private ManagementService.Client managementClient;
		private ArithmeticService.Client arithmeticClient;	
		
		public long executeOperation(String opName, int num1, int num2) throws TException, NoProviderAvailableException {
			
			ServiceProvider provider = null;
			List<ServiceProvider> providers = managementClient.getProvidersForService(opName);
			
			long result = 0;
			
			if (providers.isEmpty())
				throw new NoProviderAvailableException();
			else {
				provider = providers.get(random.nextInt(providers.size()));

				TTransport arithmeticTransport = new TSocket(provider.getHostName(), provider.getPort());
				TProtocol arithmeticProtocol = new TBinaryProtocol(arithmeticTransport);
				arithmeticClient = new ArithmeticService.Client(arithmeticProtocol);
				arithmeticTransport.open();
				
				switch(opName) {
				case "addition":
					result = arithmeticClient.add(num1, num2);
					break;
				case "multiplication":
					result = arithmeticClient.multiply(num1, num2);
					break;
				case "subtraction":
					result = arithmeticClient.substract(num1, num2);
					break;
				case "division":
					result = arithmeticClient.divide(num1, num2);
					break;
				}
				
				arithmeticTransport.close();
			}
			
			return result;
		}
		
	}