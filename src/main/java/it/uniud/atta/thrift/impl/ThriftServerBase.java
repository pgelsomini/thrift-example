package it.uniud.atta.thrift.impl;

import java.util.concurrent.CountDownLatch;

import org.apache.thrift.TProcessor;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransportException;

public abstract class ThriftServerBase implements Runnable {
	private TServer thriftServer;
	private int port;
	private volatile ServerState state;
	private CountDownLatch portLatch;
    
	public ThriftServerBase(int port) {
		this.port = port;
		this.state = ServerState.STOPPED;
		this.portLatch = new CountDownLatch(1);
	}
	
	@Override
	public final void run() {
		try {
			TServerSocket socket = new TServerSocket(port);
			port = socket.getServerSocket().getLocalPort();
			portLatch.countDown();
			thriftServer = new TThreadPoolServer(new TThreadPoolServer.Args(socket).processor(createTProcessorFromSocket(socket)));
			state = ServerState.STARTED;
			thriftServer.serve();
		} catch (TTransportException e) {
			e.printStackTrace();
		} finally {
			state = ServerState.STOPPED;
		}
	}
	
	protected abstract TProcessor createTProcessorFromSocket(TServerSocket socket);
	
	public final int start() {
		if (state == ServerState.STOPPED) {
			this.state = ServerState.STARTING;
			new Thread(this).start();
		} else
			throw new InvalidServerStateException();
		
		try {
			portLatch.await();
		} catch (InterruptedException e) {
		}
		
		return port;
	}
	
	public final void stop() {
		if (state == ServerState.STARTED || state == ServerState.STARTING) {
			state = ServerState.STOPPING;
			if (thriftServer != null)
				thriftServer.stop();
			state = ServerState.STOPPED;
		} else if (state == ServerState.STOPPING) {
			throw new InvalidServerStateException();
		} 
	}
}