package it.uniud.atta.thrift.impl;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TSocket;

import it.uniud.atta.thrift.generated.ManagementService;
import it.uniud.atta.thrift.generated.NodeManifest;

public class ArithmeticDispatcherNode implements NodeIface{

	private ThriftServer arithmeticServer;
	private ManagementService.Client managementClient;
	private NodeManifest nodeManifest;
    private OperationDispatcher dispatcher;
	
	public ArithmeticDispatcherNode(NodeManifest nodeManifest)
	{
		this.nodeManifest = nodeManifest;
		this.arithmeticServer = new ThriftServer(DispatchingServiceHandler.class);
	}
	
	@Override
	public void turnOn() throws TException { 
		//maybe change the name of turnOn() to differentiate management node's version from this and other nodes's version of the method
		
		arithmeticServer.start();
		
		TSocket arithmeticServerTransport = new TSocket("localhost", Constants.SERVER_PORT);
		TBinaryProtocol arithmeticServerProtocol = new TBinaryProtocol(arithmeticServerTransport);
		managementClient = new ManagementService.Client(arithmeticServerProtocol);
	    this.setDispatcher(new OperationDispatcher(managementClient));
		arithmeticServerTransport.open();
		
		arithmeticServer = new ThriftServer(DispatchingServiceHandler.class);
		PortNegotiator negotiator = new PortNegotiator(managementClient);
		negotiator.negotiate(nodeManifest, arithmeticServer);
		
		
	}

	@Override
	public void turnOff() {
		arithmeticServer.stop();
		
	}

	public OperationDispatcher getDispatcher() {
		return dispatcher;
	}

	public void setDispatcher(OperationDispatcher dispatcher) {
		this.dispatcher = dispatcher;
	}
	
}