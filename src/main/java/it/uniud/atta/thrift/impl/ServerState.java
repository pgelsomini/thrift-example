package it.uniud.atta.thrift.impl;

public enum ServerState {
	STOPPED,
	STARTING,
	STARTED,
	STOPPING;
}
