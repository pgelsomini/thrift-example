package it.uniud.atta.thrift.impl;

import org.apache.thrift.TException;

import it.uniud.atta.thrift.generated.ArithmeticService;

public class ArithmeticServiceHandler implements ArithmeticService.Iface {
	@Override
	public long add(int num1, int num2) throws TException {
		return num1 + num2;
	}
	
	@Override
	public long multiply(int num1, int num2) throws TException {
		return num1 * num2;
	}
	
	@Override
	public long substract(int num1, int num2) throws TException {
		return num1 - num2;
	}
	
	@Override
	public long divide(int num1, int num2) throws TException {
		return num1 / num2;
	}
	
}