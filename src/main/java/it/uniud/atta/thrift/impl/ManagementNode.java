package it.uniud.atta.thrift.impl;

import org.apache.thrift.TException;

public class ManagementNode implements NodeIface {
	
	private ThriftServer managementServer;

	public ManagementNode() {
		this.managementServer = new ThriftServer(Constants.SERVER_PORT, ManagementServiceHandler.class);
	}
	
	//maybe change name of turnOn() to differentiate management version from this nodes's version of the method
	@Override
	public void turnOn() throws TException {
		managementServer.start();
	}

	@Override
	public void turnOff() {
		managementServer.stop();
	}
}


	
