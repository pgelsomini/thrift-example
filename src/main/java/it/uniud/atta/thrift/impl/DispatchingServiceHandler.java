package it.uniud.atta.thrift.impl;

import java.util.List;
import java.util.Random;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

import it.uniud.atta.thrift.generated.ManagementService;
import it.uniud.atta.thrift.generated.ServiceProvider;
import it.uniud.atta.thrift.generated.DispatchingService;

public class DispatchingServiceHandler implements DispatchingService.Iface {
	
    private static Random random = new Random(10);
	private ManagementService.Client managementClient;
	private DispatchingService.Client dispatchingClient;
    
	@Override
	public long add(int num1, int num2) throws TException {
		return num1 + num2;
	}

	@Override
	public long multiply(int num1, int num2) throws TException {
		return num1 * num2;
	}

	@Override
	public long subtract(int num1, int num2) throws TException {
		return num1 - num2;
	}

	@Override
	public long divide(int num1, int num2) throws TException {
		return num1 / num2;
	}

	@Override
	public long executeOperation(String opName, int num1, int num2) throws TException {
		ServiceProvider provider = null;
		List<ServiceProvider> providers = managementClient.getProvidersForService(opName);
		long result = 0;
		
		if (providers.isEmpty())
			throw new NoProviderAvailableException();
		else {
			provider = providers.get(random.nextInt(providers.size()));

			TTransport dispatchingTransport = new TSocket(provider.getHostName(), provider.getPort());
			TProtocol dispatchingProtocol = new TBinaryProtocol(dispatchingTransport);
			dispatchingClient = new DispatchingService.Client(dispatchingProtocol);
			dispatchingTransport.open();
			
			switch(opName) {
			case "addition":
				result = dispatchingClient.add(num1, num2);
				break;
			case "multiplication":
				result = dispatchingClient.multiply(num1, num2);
				break;
			case "subtraction":
				result = dispatchingClient.subtract(num1, num2);
				break;
			case "division":
				result = dispatchingClient.divide(num1, num2);
				break;
			}
			
			dispatchingTransport.close();
		}
		
		return result;

	}
	
}
