package it.uniud.atta.thrift.impl;

public interface ManagementNodeIface {
    
	public void turnOn();
	
	public void turnOff();
}