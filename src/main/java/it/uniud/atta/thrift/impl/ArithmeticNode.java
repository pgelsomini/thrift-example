package it.uniud.atta.thrift.impl;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TSocket;

import it.uniud.atta.thrift.generated.ManagementService;
import it.uniud.atta.thrift.generated.NodeManifest;

public class ArithmeticNode implements NodeIface {
	
	private ThriftServer arithmeticServer;
	private ManagementService.Client managementClient;
	private NodeManifest nodeManifest;
	
	public ArithmeticNode(NodeManifest nodeManifest)
	{
		this.nodeManifest = nodeManifest;	
		this.arithmeticServer = new ThriftServer(DispatchingServiceHandler.class);
	}
	

	@Override
	public void turnOn() throws TException {
		arithmeticServer.start();
		
		TSocket arithmeticServerTransport = new TSocket("localhost", Constants.SERVER_PORT);
        TBinaryProtocol arithmeticServerProtocol = new TBinaryProtocol(arithmeticServerTransport);
        managementClient = new ManagementService.Client(arithmeticServerProtocol);
        arithmeticServerTransport.open();
        
		arithmeticServer = new ThriftServer(DispatchingServiceHandler.class);
		PortNegotiator negotiator = new PortNegotiator(managementClient);
		negotiator.negotiate(nodeManifest, arithmeticServer);
	}

	@Override
	public void turnOff() {
		arithmeticServer.stop();
	}
	
	
	

}	