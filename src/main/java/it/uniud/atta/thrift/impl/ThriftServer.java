package it.uniud.atta.thrift.impl;

import java.lang.reflect.Constructor;

import org.apache.thrift.TProcessor;
import org.apache.thrift.transport.TServerSocket;

public final class ThriftServer extends ThriftServerBase {
	 
    private final Class<?> handlerClass;
    
    public ThriftServer(Class<?> handlerClass) {
        this(0,handlerClass);
    }
    
	public ThriftServer(int port, Class<?> handlerClass) {
		super(port);
		this.handlerClass = handlerClass;
	}
	
	private Constructor<?> getServiceProcessorConstructor() {
	    
	    Constructor<?> result = null;
	    
	    Class<?> serviceClass = null;
	    Class<?> handlerInterface = null;
	    for (Class<?> interf : handlerClass.getInterfaces()) {
	        if (interf.getName().endsWith("Iface")) {
	            handlerInterface = interf;
	            serviceClass = interf.getEnclosingClass();
	            break;
	        }
	    }
	    
	    try {
    	    Class<?>[] declaredClasses = serviceClass.getDeclaredClasses(); 
    	    for (Class<?> declared : declaredClasses) {
    	        Class<?>[] implementedInterfaces = declared.getInterfaces();
    	        boolean hasTProcessor = false;
    	        for (Class<?> implementedInterface : implementedInterfaces)
    	            if (implementedInterface.equals(TProcessor.class)) {
    	                hasTProcessor = true;
    	                break;
    	            }
    	        if (hasTProcessor) {
    	            result = declared.getConstructor(handlerInterface);
    	            break;
    	        }
    	    }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	        
	    return result;
	}

	@Override
    protected TProcessor createTProcessorFromSocket(TServerSocket socket) {
	    
	    TProcessor result = null;
	    
	    try {
            result = (TProcessor)getServiceProcessorConstructor().newInstance(handlerClass.newInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }
	    
        return result;
    }
}