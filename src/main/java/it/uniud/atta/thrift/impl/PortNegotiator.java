package it.uniud.atta.thrift.impl;

import it.uniud.atta.thrift.generated.ManagementService;
import it.uniud.atta.thrift.generated.NodeManifest;
import org.apache.thrift.TException;

public class PortNegotiator {
	
	private ManagementService.Client managementClient;	
	
	public PortNegotiator(ManagementService.Client managementClient){
		this.managementClient = managementClient;
	}

	public void negotiate(NodeManifest nodeManifest, ThriftServer arithmeticServer) throws TException {
		while(true) {
			int proposedPort = arithmeticServer.start();
			int returnedPort = managementClient.registerNode(nodeManifest,proposedPort); 
			if (proposedPort != returnedPort) {
				arithmeticServer.stop();
			}
		else
			break;
		}	
	}
}