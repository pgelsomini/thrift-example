package it.uniud.atta.thrift.impl;

import org.apache.thrift.TException;

public interface NodeIface {
    
	public void turnOn() throws TException;
	
	public void turnOff();
}