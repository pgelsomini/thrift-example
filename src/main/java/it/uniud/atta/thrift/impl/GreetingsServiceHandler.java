package it.uniud.atta.thrift.impl;

import org.apache.thrift.TException;
import it.uniud.atta.thrift.generated.GreetingsService;
import it.uniud.atta.thrift.generated.Message;
import it.uniud.atta.thrift.generated.User;

public class GreetingsServiceHandler implements GreetingsService.Iface {
	
	@Override
	public Message getHello(User user) throws TException {
		Message answer = new Message();
		answer = answer.setMessage("Ciao " + user.getName() + ", welcome among us!");
		return answer;
	}
}