package it.uniud.atta.velocity.impl;

import java.util.*;

public class ClassDescriptor {
	
	private String name;
	private ArrayList attributes = new ArrayList();
	private ArrayList methods = new ArrayList();
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void addAttribute(AttributeDescriptor attribute) {
		attributes.add(attribute);
	}
	public ArrayList getAttributes() {
		return attributes;
	}
	
	public void addMethod(MethodDescriptor method) {
		methods.add(method);
	}
	
	public ArrayList getMethods() {
		return methods;
	}
}