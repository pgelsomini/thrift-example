package it.uniud.atta.thrift;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.junit.*;

import it.uniud.atta.thrift.generated.*;
import it.uniud.atta.thrift.impl.ManagementNode;
import it.uniud.atta.thrift.impl.ManagementServiceHandler;
import it.uniud.atta.thrift.impl.ThriftServer;


public class ManagementServiceTest {
	
	private ThriftServer managementServer;
	private TTransport managementServerTransport;
	private TProtocol managementServerProtocol;
	private ManagementService.Client managementClient;

	@Before
	public void initializeManagementServer() throws Exception {
		
		managementServer = new ThriftServer(7911,ManagementServiceHandler.class);
		managementServer.start();
        	
    	Thread.sleep(200);
        managementServerTransport = new TSocket("localhost", 7911);
        managementServerProtocol = new TBinaryProtocol(managementServerTransport);
        managementClient = new ManagementService.Client(managementServerProtocol);
        managementServerTransport.open();
	}
	
	@Test
	public void testRegistration() throws Exception {
		
		List<ServiceProvider> serviceProviders;
		
		List<String> askingNodeSvc = new ArrayList<>();
		askingNodeSvc.add("addition");
		
		List<String> answeringNodeSvc = new ArrayList<>();
		answeringNodeSvc.add("substraction");
		answeringNodeSvc.add("multiplication");
		
		List<String> thirdNodeSvc = new ArrayList<>();
		thirdNodeSvc.add("addition");
		thirdNodeSvc.add("multiplication");
		thirdNodeSvc.add("Division");
		
		NodeManifest askingNode = new NodeManifest("Asking Node", askingNodeSvc);
		int port = managementClient.registerNode(askingNode,1026);
		
		NodeManifest answeringNode = new NodeManifest("Answering Node", answeringNodeSvc);
		port = managementClient.registerNode(answeringNode,1027);
		assertEquals(1027, port);
		
		serviceProviders = managementClient.getProvidersForService("substraction");
		assertEquals(1, serviceProviders.size());
		
		NodeManifest otherNode = new NodeManifest("Other Node", thirdNodeSvc);
		port = managementClient.registerNode(otherNode,1028);
		assertEquals(1028, port);
		
		serviceProviders = managementClient.getProvidersForService("addition");
		assertEquals(2, serviceProviders.size());
	}
	
	@After
	public void teardownManagementServer() throws Exception {
		managementServerTransport.close();
		managementServer.stop();			
	}
	
	@Test
	public void testManagementNode() throws Exception {
		ManagementNode servingNode = new ManagementNode();
		servingNode.turnOn();
		servingNode.turnOff();
	}
	
	@Test
	public void testRegistrationToNewServer() throws Exception {
		
		List<ServiceProvider> serviceProviders;
		List<String> askingNodeSvc = new ArrayList<>();
		askingNodeSvc.add("addition");
		
		List<String> answeringNodeSvc = new ArrayList<>();
		answeringNodeSvc.add("subtraction");
		answeringNodeSvc.add("multiplication");
		
		List<String> thirdNodeSvc = new ArrayList<>();
		thirdNodeSvc.add("addition");
		thirdNodeSvc.add("multiplication");
		thirdNodeSvc.add("Division");
		
		NodeManifest askingNode = new NodeManifest("Asking Node", askingNodeSvc);
		int port = managementClient.registerNode(askingNode,1026);
		
		NodeManifest answeringNode = new NodeManifest("Answering Node", answeringNodeSvc);
		port = managementClient.registerNode(answeringNode,1027);
		assertEquals(1027, port);
		
		serviceProviders = managementClient.getProvidersForService("subtraction");
		assertEquals(1, serviceProviders.size());
		
		NodeManifest otherNode = new NodeManifest("Other Node", thirdNodeSvc);
		port = managementClient.registerNode(otherNode,1028);
		assertEquals(1028, port);
		
		serviceProviders = managementClient.getProvidersForService("multiplication");
		assertEquals(2, serviceProviders.size());
	}

}


