package it.uniud.atta.thrift;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.junit.*;

import it.uniud.atta.thrift.generated.*;
import it.uniud.atta.thrift.impl.ArithmeticNode;
import it.uniud.atta.thrift.impl.Constants;
import it.uniud.atta.thrift.impl.ManagementNode;


public class ArithmeticNodeTest {
	
	private ManagementNode managementNode;
	private TTransport managementNodeTransport;
	private TProtocol managementNodeProtocol;
	
    @Before
	public void initializeManagementNode() throws Exception {
		
		managementNode = new ManagementNode();
		managementNode.turnOn();
        	
    	Thread.sleep(200);
        managementNodeTransport = new TSocket("localhost", Constants.SERVER_PORT);
        managementNodeProtocol = new TBinaryProtocol(managementNodeTransport);
        new ManagementService.Client(managementNodeProtocol);
        managementNodeTransport.open();
	}
	
	@Test 
	public void testRegistration() throws Exception {
		
		List<NodeManifest> nodeManifests = new ArrayList<>();
		nodeManifests.add(new NodeManifest("localhost", Arrays.asList(new String[]{"addition"})));
		nodeManifests.add(new NodeManifest("localhost", Arrays.asList(new String[]{"subtraction","multiplication"})));
		nodeManifests.add(new NodeManifest("localhost", Arrays.asList(new String[]{"addition","multiplication","division"})));
		nodeManifests.add(new NodeManifest("localhost", Arrays.asList(new String[]{"addition","division"})));
		
		List<ArithmeticNode> arithmeticNodes = new ArrayList<>();
		
		
		for (NodeManifest nodeManifest : nodeManifests) {
			ArithmeticNode arithmeticNode = new ArithmeticNode(nodeManifest);
			arithmeticNodes.add(arithmeticNode);
			arithmeticNode.turnOn();
		}
	}
	
	@After
	public void teardownManagementServer() throws Exception {
		managementNodeTransport.close();
		managementNode.turnOff();			
	}
}

