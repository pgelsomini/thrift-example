package it.uniud.atta.thrift;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.junit.*;

import it.uniud.atta.thrift.generated.*;
import it.uniud.atta.thrift.impl.*;


public class NoOperationNodeTest{
	
	private ThriftServer managementServer;
	private ManagementService.Client managementClient;
	private TTransport managementServerTransport;
	private TProtocol managementServerProtocol;
	
	@Before
	public void initializeManagementServer() throws Exception {
		
		managementServer = new ThriftServer(7911,ManagementServiceHandler.class);
		managementServer.start();
        	
    	Thread.sleep(200);
        managementServerTransport = new TSocket("localhost", 7911);
        managementServerProtocol = new TBinaryProtocol(managementServerTransport);
        managementClient = new ManagementService.Client(managementServerProtocol);
        managementServerTransport.open();
	}
	
	@Test
	public void testNoOperationNode() throws Exception {
		
		List<NodeManifest> nodeManifests = new ArrayList<>();
		nodeManifests.add(new NodeManifest("localhost", Arrays.asList(new String[]{""})));
		
		List<ThriftServer> arithmeticServers = new ArrayList<>();
		
		for (NodeManifest nodeManifest : nodeManifests) {
			ThriftServer arithmeticServer = new ThriftServer(ArithmeticServiceHandler.class);
			arithmeticServers.add(arithmeticServer);
			negotiatePort(nodeManifest,arithmeticServer);
		}
		
		assertEquals(1, nodeManifests.size());
		assertEquals(1, arithmeticServers.size());
	}
	
	private void negotiatePort(NodeManifest nodeManifest, ThriftServer arithmeticServer) throws TException {
			while(true) {
				int proposedPort = arithmeticServer.start();
				int returnedPort = managementClient.registerNode(nodeManifest,proposedPort); 
				if (proposedPort != returnedPort) {
					arithmeticServer.stop();
				}
				else
					break;
			}
		}
	
	@After
	public void teardownManagementServer() throws Exception {
		managementServerTransport.close();
		managementServer.stop();			
	}
}