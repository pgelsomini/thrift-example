package it.uniud.atta.thrift;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.junit.*;

import it.uniud.atta.thrift.generated.*;
import it.uniud.atta.thrift.impl.*;

public class PortNegotiatorTest{
	
	private ThriftServer managementServer;
	private ManagementService.Client managementClient;
	private TTransport managementServerTransport;
	private TProtocol managementServerProtocol;
	
	@Before
	public void initializeManagementServer() throws Exception {
		
		managementServer = new ThriftServer(7911,ManagementServiceHandler.class);
		managementServer.start();
        	
    	Thread.sleep(200);
        managementServerTransport = new TSocket("localhost", 7911);
        managementServerProtocol = new TBinaryProtocol(managementServerTransport);
        managementClient = new ManagementService.Client(managementServerProtocol);
        managementServerTransport.open();
	}
	
	@Test
	public void testNegotiate() throws TException {
		List<NodeManifest> nodeManifests = new ArrayList<>();
		nodeManifests.add(new NodeManifest("localhost", Arrays.asList(new String[]{"addition"})));
		nodeManifests.add(new NodeManifest("localhost", Arrays.asList(new String[]{"subtraction","multiplication"})));
		nodeManifests.add(new NodeManifest("localhost", Arrays.asList(new String[]{"addition","multiplication","division"})));
		nodeManifests.add(new NodeManifest("localhost", Arrays.asList(new String[]{"addition","division"})));
		
		List<ThriftServer> arithmeticServers = new ArrayList<>();
		
		for (NodeManifest nodeManifest : nodeManifests) {
			ThriftServer arithmeticServer = new ThriftServer(DispatchingServiceHandler.class);
			arithmeticServers.add(arithmeticServer);
			PortNegotiator negotiator = new PortNegotiator(managementClient);
			negotiator.negotiate(nodeManifest, arithmeticServer);
		}
	}
}
