package it.uniud.atta.thrift;


import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.junit.*;

import it.uniud.atta.thrift.generated.*;
import it.uniud.atta.thrift.impl.*;

public class ArithmeticDispatcherNodeTest{
	
	private ManagementNode managementNode;
	private TTransport managementNodeTransport;
	private TProtocol managementNodeProtocol;
	
    @Before
	public void initializeManagementNode() throws Exception {
		
		managementNode = new ManagementNode();
		managementNode.turnOn();
        	
    	Thread.sleep(200);
        managementNodeTransport = new TSocket("localhost", Constants.SERVER_PORT);
        managementNodeProtocol = new TBinaryProtocol(managementNodeTransport);
        new ManagementService.Client(managementNodeProtocol);
        managementNodeTransport.open();
	}
	
	@Test
	public void testNodeRegistration() throws Exception {
		
		List<NodeManifest> nodeManifests = new ArrayList<>();
		nodeManifests.add(new NodeManifest("localhost", Arrays.asList(new String[]{"addition"})));
		nodeManifests.add(new NodeManifest("localhost", Arrays.asList(new String[]{"subtraction","multiplication"})));
		nodeManifests.add(new NodeManifest("localhost", Arrays.asList(new String[]{"addition","multiplication","division"})));
		nodeManifests.add(new NodeManifest("localhost", Arrays.asList(new String[]{"addition","division"})));
		
		List<ArithmeticNode> arithmeticNodes = new ArrayList<>();
		
		
		for (NodeManifest nodeManifest : nodeManifests) {
			ArithmeticNode arithmeticNode = new ArithmeticNode(nodeManifest);
			arithmeticNodes.add(arithmeticNode);
			arithmeticNode.turnOn();
		}
		
		assertEquals(4, arithmeticNodes.size());
		
		
		
		for (ArithmeticNode arithmeticNode : arithmeticNodes) {
			arithmeticNode.turnOff();
		}
	
	}
	
	@Test
	public void testDispatchingService() throws Exception {
		
		List<ArithmeticDispatcherNode> nodes = new ArrayList<>();
		
		List<String> node1Svc = new ArrayList<>();
		node1Svc.add("addition");
		node1Svc.add("subtraction");
		
		List<String> node2Svc = new ArrayList<>();
		node2Svc.add("");
		
		NodeManifest node1 = new NodeManifest("localhost", node1Svc);
		NodeManifest node2 = new NodeManifest("localhost", node2Svc);
		
		assertEquals(0, nodes.size());
		
		ArithmeticDispatcherNode first = new ArithmeticDispatcherNode(node1);
		nodes.add(first);
		first.turnOn();		
		
		ArithmeticDispatcherNode second = new ArithmeticDispatcherNode(node2);
		nodes.add(second);
		second.turnOn();
		
		assertEquals(2, nodes.size());
		
		//the following line causes a nullPointerException
		
		long result = second.getDispatcher().executeOperation("addition", 1, 2);
		assertEquals(3, result);
		
		first.turnOff();
		second.turnOff();
		
	}
	
	@After
	public void teardownManagementServer() throws Exception {
		managementNodeTransport.close();
		managementNode.turnOff();			
	}
}