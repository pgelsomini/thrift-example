package it.uniud.atta.thrift;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.junit.*;

import it.uniud.atta.thrift.generated.*;
import it.uniud.atta.thrift.impl.*;

public class GreetingsNodeTest{
	
	private ManagementNode managementNode;
	private TTransport managementNodeTransport;
	private TProtocol managementNodeProtocol;
	
	@Before
	public void initializeManagementNode() throws Exception {
		
		managementNode = new ManagementNode();
		managementNode.turnOn();
        	
    	Thread.sleep(200);
        managementNodeTransport = new TSocket("localhost", Constants.SERVER_PORT);
        managementNodeProtocol = new TBinaryProtocol(managementNodeTransport);
        new ManagementService.Client(managementNodeProtocol);
        managementNodeTransport.open();
	}
    
    @Test
    public void testNodeRegistration() throws TException {
    	
		List<GreetingsNode> nodes = new ArrayList<>();
    	
		List<String> node1Svc = new ArrayList<>();
		node1Svc.add("saluting");
		List<String> node2Svc = new ArrayList<>();
		node2Svc.add("welcomer");
	
		NodeManifest node1 = new NodeManifest("localhost", node1Svc);
		NodeManifest node2 = new NodeManifest("localhost", node2Svc);  	
    	
    	GreetingsNode paolo = new GreetingsNode(node1, "Paolo");
    	GreetingsNode luca = new GreetingsNode(node2, "Luca");

    	nodes.add(paolo);
    	paolo.turnOn();
    	
    	nodes.add(luca);
    	luca.turnOn();
    	
    	assertEquals("Paolo", paolo.getUser().name);
    	assertEquals("Luca", luca.getUser().name);
    	assertEquals(2, nodes.size());   
    	
    	paolo.turnOff();
    	luca.turnOff();
    }
    
    @Test(expected = NoProviderAvailableException.class)
    public void testNoProviderAvailableException() throws Exception {
    	
    	List<GreetingsNode> nodes = new ArrayList<>();
    	List<String> svc1 = new ArrayList<>();
    	svc1.add("saluting");
    	List<String> svc2 = new ArrayList<>();
    	svc2.add("welcomer");
    	
    	NodeManifest node1 = new NodeManifest("localhost", svc1);
    	NodeManifest node2 = new NodeManifest("localhost", svc2);
    	
    	GreetingsNode paolo = new GreetingsNode(node1, "Paolo");
    	nodes.add(paolo);
    	paolo.turnOn();
    	
    	GreetingsNode luca = new GreetingsNode(node2, "Luca");
    	//node "Luca" doesn't add its service to nodes
    	luca.turnOn();
    	
    	assertEquals("Paolo", paolo.getUser().name);
    	assertEquals(1, nodes.size());
    	assertEquals(luca.getMessenger().callOtherNode(paolo.getUser()).getMessage(), "Ciao Paolo, welcome among us!");
    }
    
    @Test
    public void testNodeSalutation() throws TException {
    	List<GreetingsNode> nodes = new ArrayList<>();
    	
    	List<String> node1Svc = new ArrayList<>();
    	node1Svc.add("Paolo");
    	List<String> node2Svc = new ArrayList<>();
    	node2Svc.add("Luca");
    	
    	NodeManifest node1 = new NodeManifest("localhost", node1Svc);
    	NodeManifest node2 = new NodeManifest("localhost", node2Svc);
    	
    	GreetingsNode paolo = new GreetingsNode(node1, "Paolo");
    	GreetingsNode luca = new GreetingsNode(node2, "Luca");
    	
    	nodes.add(paolo);
    	paolo.turnOn();
    	nodes.add(luca);
    	luca.turnOn();
    	
    	assertEquals("Paolo", paolo.getUser().name);
    	assertEquals("Luca", luca .getUser().name);
    	
    	
    	
    	//need to change generic "client" with the actual GreetingsNode client
    	assertEquals(luca.getMessenger().callOtherNode(paolo.getUser()).getMessage(), "Ciao Paolo, welcome among us!");
    	
    	
    	paolo.turnOff();
    	luca.turnOff();
    			
    }
    
	@After
	public void teardownManagementServer() throws Exception {
		managementNodeTransport.close();
		managementNode.turnOff();			
	}
   
}