package it.uniud.atta.thrift;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.junit.*;

import it.uniud.atta.thrift.generated.*;
import it.uniud.atta.thrift.impl.Constants;
import it.uniud.atta.thrift.impl.ManagementNode;


public class ManagementNodeTest {
	
	private ManagementNode managementNode;
	private ManagementService.Client managementClient;
	private TTransport managementServerTransport;
	private TProtocol managementServerProtocol;

	@Before
	public void initialize() throws Exception {
		
		managementNode = new ManagementNode();
		managementNode.turnOn();
        	
    	Thread.sleep(200);
        managementServerTransport = new TSocket("localhost", Constants.SERVER_PORT);
        managementServerProtocol = new TBinaryProtocol(managementServerTransport);
        managementClient = new ManagementService.Client(managementServerProtocol);
        managementServerTransport.open();
	}
	
	@Test
	public void testRegistration() throws Exception {
		
		List<ServiceProvider> serviceProviders;
		
		List<String> askingNodeSvc = new ArrayList<>();
		askingNodeSvc.add("addition");
		
		List<String> answeringNodeSvc = new ArrayList<>();
		answeringNodeSvc.add("substraction");
		answeringNodeSvc.add("multiplication");
		
		List<String> thirdNodeSvc = new ArrayList<>();
		thirdNodeSvc.add("addition");
		thirdNodeSvc.add("multiplication");
		thirdNodeSvc.add("Division");
		
		NodeManifest askingNode = new NodeManifest("Asking Node", askingNodeSvc);
		int port = managementClient.registerNode(askingNode,1026);
		
		NodeManifest answeringNode = new NodeManifest("Answering Node", answeringNodeSvc);
		port = managementClient.registerNode(answeringNode,1027);
		assertEquals(1027, port);
		
		serviceProviders = managementClient.getProvidersForService("substraction");
		assertEquals(1, serviceProviders.size());
		
		NodeManifest otherNode = new NodeManifest("Other Node", thirdNodeSvc);
		port = managementClient.registerNode(otherNode,1028);
		assertEquals(1028, port);
	}
	
	@After
	public void teardownManagementServer() throws Exception {
		managementServerTransport.close();
		managementNode.turnOff();			
	}
	
	@Test
	public void testRegistrationToNewServer() throws Exception {
		
		List<ServiceProvider> serviceProviders;
		List<String> askingNodeSvc = new ArrayList<>();
		askingNodeSvc.add("addition");
		
		List<String> answeringNodeSvc = new ArrayList<>();
		answeringNodeSvc.add("substraction");
		answeringNodeSvc.add("multiplication");
		
		List<String> thirdNodeSvc = new ArrayList<>();
		thirdNodeSvc.add("addition");
		thirdNodeSvc.add("multiplication");
		thirdNodeSvc.add("Division");
		
		NodeManifest askingNode = new NodeManifest("Asking Node", askingNodeSvc);
		int port = managementClient.registerNode(askingNode,1026);
		
		NodeManifest answeringNode = new NodeManifest("Answering Node", answeringNodeSvc);
		port = managementClient.registerNode(answeringNode,1027);
		assertEquals(1027, port);
		
		serviceProviders = managementClient.getProvidersForService("substraction");
		assertEquals(1, serviceProviders.size());
		
		NodeManifest otherNode = new NodeManifest("Other Node", thirdNodeSvc);
		port = managementClient.registerNode(otherNode,1028);
		assertEquals(1028, port);
		
		serviceProviders = managementClient.getProvidersForService("multiplication");
		assertEquals(2, serviceProviders.size());		
	}

}


