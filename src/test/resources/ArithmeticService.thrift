namespace java it.uniud.atta.thrift.generated  // define namespace for java code

typedef i32 int
typedef i64 long


service ArithmeticService {
	long add(1: int num1, 2: int num2),
	long multiply(1: int num1, 2: int num2),
	long substract(1: int num1, 2: int num2),
	long divide(1: int num1, 2: int num2)
}

