namespace java it.uniud.atta.thrift.generated  // define namespace for java code

struct NodeManifest {
	1: string hostName,
	2: list<string> serviceList
}
	
struct ServiceProvider {
	1: string hostName,
	2: i32 port
}

typedef i32 port
typedef i32 integer
typedef i32 int
typedef i64 long
typedef string hostName
typedef list<ServiceProvider> serviceProviders
typedef string svcName

service ManagementService {
	port registerNode(1: NodeManifest hostAndService, 2: port proposedPort),
	void unRegister(1: NodeManifest hostAndService),
	serviceProviders getProvidersForService(1: svcName serviceName),
	list<NodeManifest> getAllNodes(),
}
