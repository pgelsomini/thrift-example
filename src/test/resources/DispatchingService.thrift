namespace java it.uniud.atta.thrift.generated  // define namespace for java code

typedef i32 int
typedef i64 long

service DispatchingService {
	long add(1: int num1, 2: int num2),
	long multiply(1: int num1, 2: int num2),
	long subtract(1: int num1, 2: int num2),
	long divide(1: int num1, 2: int num2)
	
	long executeOperation(1: string opName, 2: int num1, 3: int num2),
}



