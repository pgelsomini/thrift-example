namespace java it.uniud.atta.thrift.generated  // define namespace for java code

struct Message {
	1: string message
}

struct User {
	1: string name
}

service GreetingsService {
	Message getHello(1: User user),
}